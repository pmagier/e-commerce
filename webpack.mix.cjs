let mix = require("laravel-mix");

mix.copy(
    "resources/frontend/dist/spa/index.html",
    "resources/views/app.blade.php"
).copyDirectory("resources/frontend/dist/spa", "public");
