import { defineStore } from 'pinia';

export const useBasketStore = defineStore('basket', {
    state: () =>({
        itemsCount: 0,
        items: []
    }),
    getters: {

    },
    actions:{
        increment(){
            this.itemsCount++;
        }
    }
}) 