import { defineStore } from "pinia";

export const useProductStore = defineStore("product", {
  state: () => ({
    name: "",
    products: {},
  }),
  getters: {
    getProducts: (state, name) => state.products[name]
  },
  actions: {
    fetchProducts(name) {
      api
        .get(process.env.API + name) // htttp://e-commerce.gomedia/api/{category}
        .then((response) => {
          this.products[name] = response.data;
        })
        .catch(() => {
          console.log("Failed to fetch");
        });
    },
  },
});
