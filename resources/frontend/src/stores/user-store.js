import { defineStore } from "pinia";

export const useUserStore = defineStore("user", {
  state: () => ({
    loggedIn: true,
  }),
  getters: {},
  actions: {},
});
