
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/docs', component: () => import('pages/ProductsPage.vue') },
      { path: '/github', component: () => import('pages/ProductsPage.vue') },
      { path: '/discord', component: () => import('pages/ProductsPage.vue') },
      { path: '/forum', component: () => import('pages/ProductsPage.vue') },
      { path: '/basket', component: () => import('pages/BasketPage.vue') },
      { path: '/product', component: () => import('pages/ProductPage.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
