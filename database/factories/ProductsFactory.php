<?php

namespace Database\Factories;

use App\Models\Products;
use App\Models\Categories;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Products>
 */
class ProductsFactory extends Factory
{
    protected $model = Products::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = rtrim($this->faker->sentence(rand(1,3)), ".");
        return [
            'name' => $name,
            'slug' => strtolower(str_replace(" ", "-",$name)),
            'desc' => $this->faker->sentence(rand(10,20)),
            'price' => rand(50,100),
            'category_id' => function(){
                return Categories::inRandomOrder()->first()->id;
            }
        ];
    }
}
