<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products = Products::all();
        return response()->json($products);
    }

    public function show($id){
        $product = Products::find($id);

        if(empty($product)){
            return response()->json(['message' => 'Product not found'], 404);
        }

        return response()->json($product);
    }

    public function showByCategories($id){
        $category = Categories::find($id);
        $products = Products::where('category_id', $category->id)->get();

        return response()->json($products);

    }

    public function update($id, Request $request){
        $product = Products::find($id);
        $product->votes += 1;
        $product->total_rating += $request->rating;
        $product->save();

        return response()->json(['message' => 'Vote has been submitted']);
    }
}
